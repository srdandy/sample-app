import { describe, expect, test } from '@jest/globals';
import { getEndpointByType, API_ENDPOINT, getEndpointBy } from '.';

describe('get endpoint by type', () => {
  const typeA = 'plant'
  
  test('it should return the correct url when adding a type product', () => {
    const result = `${API_ENDPOINT}?product_type=${typeA}`
    expect(getEndpointByType(typeA)).toBe(result)
  });
});

describe('get endpoint by', () => {
    const type = 'a', 
    brand = 'b', 
    priceHigher = 5, 
    priceLower = 6, 
    ratingHigher = 10, 
    ratingLower = 4
    
    test('it should return the correct url when nothing is added', () => {
      expect(getEndpointBy({})).toBe(API_ENDPOINT)
    });

    test('it should return the correct url when adding type', () => {
        const result = `${API_ENDPOINT}?product_type=${type}`
        expect(getEndpointBy({ type })).toBe(result)
    });

    test('it should return the correct url when adding brand', () => {
        const result = `${API_ENDPOINT}?brand=${brand}`
        expect(getEndpointBy({ brand })).toBe(result)
    });

    test('it should return the correct url when adding price higher', () => {
        const result = `${API_ENDPOINT}?price_greater_than=${priceHigher}`
        expect(getEndpointBy({ priceHigher })).toBe(result)
    });

    test('it should return the correct url when adding price lower', () => {
        const result = `${API_ENDPOINT}?price_less_than=${priceLower}`
        expect(getEndpointBy({ priceLower })).toBe(result)
    });

    test('it should return the correct url when adding rating higher', () => {
        const result = `${API_ENDPOINT}?rating_greater_than=${ratingHigher}`
        expect(getEndpointBy({ ratingHigher })).toBe(result)
    });

    test('it should return the correct url when adding rating lower', () => {
        const result = `${API_ENDPOINT}?rating_less_than=${ratingLower}`
        expect(getEndpointBy({ ratingLower })).toBe(result)
    });

    test('it should return the correct url when adding type and brand', () => {
        const result = `${API_ENDPOINT}?product_type=${type}&brand=${brand}`
        expect(getEndpointBy({ type, brand })).toBe(result)
    });

    test('it should return the correct url when adding multiple params', () => {
        const result = `${API_ENDPOINT}?product_type=${type}&brand=${brand}&price_greater_than=${priceHigher}&rating_less_than=${ratingLower}`
        expect(getEndpointBy({ type, brand, priceHigher, ratingLower })).toBe(result)
    });
  });