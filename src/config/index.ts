import { Filters } from "../types/api"
import { addParams } from "../utils"

export const API_ENDPOINT = 'https://makeup-api.herokuapp.com/api/v1/products.json'

export const getEndpointByType = (type: string) => {
    return `${API_ENDPOINT}?product_type=${type}`
}

export const getEndpointBy = ({ type, brand, priceHigher, priceLower, ratingHigher, ratingLower }: Filters) => {
    let baseUrl = `${API_ENDPOINT}`

    if(type && type.length) baseUrl = addParams(baseUrl)('product_type', type)
    if(brand && brand.length) baseUrl = addParams(baseUrl)('brand', brand)
    if(priceHigher && priceHigher > 0) baseUrl = addParams(baseUrl)('price_greater_than', priceHigher)
    if(priceLower && priceLower > 0) baseUrl = addParams(baseUrl)('price_less_than', priceLower)
    if(ratingHigher && ratingHigher > 0) baseUrl = addParams(baseUrl)('rating_greater_than', ratingHigher)
    if(ratingLower && ratingLower > 0) baseUrl = addParams(baseUrl)('rating_less_than', ratingLower)

    return baseUrl
}