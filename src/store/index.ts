import create from 'zustand'
import { persist } from 'zustand/middleware';
import { Product } from '../types/product'
import { StoreState } from '../types/store'

export const productStore = create<StoreState>()(persist((set, get) => ({
  products: [] as Product[],
  setProducts: (products) => set(() => ({ products })),
  getProducts: () => get().products,
  getProductById: (id) => get().products.find(product => product.id === id),
  getProductsByName: (name) => get().products.filter(product => product.name.includes(name)),
})))

