import { Product } from './product'

export type StoreState = {
    products: Product[]
    setProducts: (products: Product[]) => void
    getProductById: (id: number) => Product | undefined
    getProductsByName: (name: string) => Product[]
    getProducts: () => Product[]
}
