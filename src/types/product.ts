export type Product = {
    id: number
    name: string
    brand: string
    price: string | number
    image_link: string
    product_api_url: string
    product_colors: ProductColor[]
    description: string
}

export type ProductColor = {
    hex_value: string
    colour_name: string
} 