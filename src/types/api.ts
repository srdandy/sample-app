export type Filters = {
    type?:string
    brand?:string
    priceHigher?:number
    priceLower?:number
    ratingHigher?:number
    ratingLower?:number
}