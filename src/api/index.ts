import useSWR from 'swr'
import { fetcher } from "../utils";
import { API_ENDPOINT, getEndpointByType, getEndpointBy } from '../config';
import { Filters } from '../types/api';

export const useProducts = (shouldFetch = true) => {
    const { data, error } = useSWR(shouldFetch ? API_ENDPOINT: null, fetcher)
    return {
        products: data,
        loading: !error && !data && shouldFetch,
        error: error
    }
}

export const getProductsByType = (type: string) => {
    return fetch(getEndpointByType(type))
        .then(response => response.json())
        .catch(e => e)
}

export const getProductsBy = ({ type, brand, priceHigher, priceLower, ratingHigher, ratingLower }: Filters) => {
    return fetch(getEndpointBy({ type, brand, priceHigher, priceLower, ratingHigher, ratingLower }))
        .then(response => response.json())
        .catch(e => e)
}