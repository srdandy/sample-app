import axios from 'axios'

export const fetcher = (url: string) => axios.get(url).then(res => res.data)

export const addParams = (baseUrl: string) => (key: string, value: string | number) => {
    if(!key || !key.length || !value) return baseUrl
    if(typeof value === 'string' && !value.length) return baseUrl
    if(typeof value === 'number' && value <= 0) return baseUrl
    if(!baseUrl.includes('?')) return `${baseUrl}?${key}=${value}`
    return `${baseUrl}&${key}=${value}`
}