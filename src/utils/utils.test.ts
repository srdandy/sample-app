import {describe, expect, test} from '@jest/globals';
import { addParams } from '.';

describe('add params', () => {
  const baseUrl = "http://www.google.com"
  const paramA = 'a'
  const paramB = 'b'
  
  test('it should return the base url when there are any empty parameters', () => {
    expect(addParams(baseUrl)('', 'test')).toBe(baseUrl)
    expect(addParams(baseUrl)('test', '')).toBe(baseUrl)
    expect(addParams(baseUrl)('', -2)).toBe(baseUrl)
    expect(addParams(baseUrl)('', 0)).toBe(baseUrl)
  });

  test('it should return a valid url when there is at least one parameter', () => {
    const result = `${baseUrl}?${paramA}=${paramA}`
    expect(addParams(baseUrl)(paramA, paramA)).toBe(result)
  });

  test('it should return a valid url when there is there are more than one parameter', () => {
    const firstUrl = `${baseUrl}?${paramA}=${paramA}`
    const result = `${firstUrl}&${paramB}=${paramB}`
    expect(addParams(firstUrl)(paramB, paramB)).toBe(result)
  });
});