import React from "react"

interface Props {
    text?: string
}

export const Loader = ({ text = 'Loading'}: Props) => {
    return (
    <div className="flex justify-center items-center">
        <div className="spinner-border animate-spin inline-block w-8 h-8 border-4 rounded-full" role="status" />
        <span className="visually-hidden">{text}</span>
    </div>
    )
}