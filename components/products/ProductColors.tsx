import React from 'react'
import { ProductColor } from '../../src/types/product'

interface Props {
    colors: ProductColor[]
}

export const ProductColors = ({ colors }: Props) => {
    const partialColors = colors.splice(0, 5)
    if(!partialColors.length) return null
    return (
    <div>
        <h4 className="text-sm font-medium text-gray-900">Available colors</h4>
        <ul className="flex items-center space-x-3">
            { partialColors.map((color, index) => (
                <li key={index} className='rounded-full p-2' style={{ backgroundColor: `${color.hex_value}`}} />
            ))
            }
        </ul>
      </div>
    )
}