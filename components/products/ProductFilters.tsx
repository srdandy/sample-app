import React, { ChangeEventHandler} from "react"
import { Search } from "../elements/Search"

interface Props {
    onChangeName: ChangeEventHandler
    onChangeBrand: ChangeEventHandler
    onChangeType: ChangeEventHandler
    onChangePriceGreat: ChangeEventHandler
    onChangePriceLower: ChangeEventHandler
    onChangeRatingGreat: ChangeEventHandler
    onChangeRatingLower: ChangeEventHandler
}

export const ProductFilters = ({ onChangeBrand, onChangeName, onChangePriceGreat, onChangePriceLower, onChangeRatingGreat, onChangeRatingLower, onChangeType} : Props) => {
    return (
        <div className="mt-10 sm:mt-0">
            <div className="md:grid md:grid-cols-3 md:gap-6">
            <div className="md:col-span-6">
                <div className="px-4 sm:px-0">
                <h3 className="text-lg font-medium leading-6 text-gray-900">Product list search</h3>
                <p className="mt-1 text-sm text-gray-600">Search by different categories</p>
                </div>
            </div>
            </div>
            
            <div className="mt-5 md:col-span-2 md:mt-0">
            <form action="#" method="POST">
                <div className="overflow-hidden shadow sm:rounded-md">
                    <div className="bg-white px-4 py-5 sm:p-6">
                        <div className="grid grid-cols-6 gap-6">
                            <div className="col-span-6">
                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                    Name
                                </label>
                                <Search onChange={onChangeName} />
                            </div>
                            <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                    Brand
                                </label>
                                <Search onChange={onChangeBrand} />
                            </div>
                            <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                    Type
                                </label>
                                <Search onChange={onChangeType} />
                            </div>
                        </div>
                        <div className="grid grid-cols-6 gap-6">
                            <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                    Price greater than
                                </label>
                                <Search type="number" onChange={onChangePriceGreat} />
                            </div>
                            <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                    Price lower than
                                </label>
                                <Search type="number" onChange={onChangePriceLower} />
                            </div>
                        </div>
                        <div className="grid grid-cols-6 gap-6">
                            <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                    Rating greater than
                                </label>
                                <Search type="number" onChange={onChangeRatingGreat} />
                            </div>
                            <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                    Rating lower than
                                </label>
                                <Search type="number" onChange={onChangeRatingLower} />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    )
}