import { Product } from "../../src/types/product"
import { ProductColors } from "./ProductColors"

interface Props {
    product: Product
}

export const ProductItem = ({ product }: Props) => {
    const price = Intl.NumberFormat('en-US', { currency: 'USD', style: 'currency' }).format(Number(product.price));
    return (
        <div className="group relative">
            <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-md bg-gray-200 group-hover:opacity-75 lg:aspect-none lg:h-80">
            <img
                src={product.image_link}
                alt={`${product.name} by ${product.brand}`}
                className="h-full w-full object-cover object-center lg:h-full lg:w-full"
            />
            </div>
            <div className="mt-4 flex justify-between">
            <div>
                <h3 className="text-sm text-gray-700">
                <a href={`/product/${product.id}`}>
                    <span aria-hidden="true" className="absolute inset-0" />
                    {product.name}
                </a>
                </h3>
                <ProductColors colors={product.product_colors} />
            </div>
            <p className="text-sm font-medium text-gray-900">{price}</p>
            </div>
        </div>
    )
}