import React from "react"

interface Props {
    price: string | number,
}

export const ProductPrice= ({ price }: Props) => {
    const formattedPrice = Intl.NumberFormat('en-US', { currency: 'USD', style: 'currency' }).format(Number(price));
    return (
        <div className="mx-auto mt-6 max-w-2xl sm:px-6 lg:grid lg:max-w-7xl lg:grid-cols-3 lg:gap-x-8 lg:px-8">
          <div className="mt-4 lg:row-span-3 lg:mt-0">
            <h2 className="sr-only">Product information</h2>
            <p className="text-3xl tracking-tight text-gray-900">{formattedPrice}</p>
          </div>
        </div>
    )
}