This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Technical Decisions

**Disclaimer**

Some new tools were used beyond my expertise for simplicty, fast development and to learn new things while investing time on an assignment
A win-win situation for both parties

**Tools**

- Ts-Jest for testing with typescript
- Zustand for easy state management without boilerplate, fast and straightforward
- Tailwind for easy out of the box implementations

**Further Improvements**

- Improve the intial load of the page becasue of a slow api response
- Add a better design to render something initially for the customer and then load the products
- Split the list of products to improve the loading time - api side
- Improve the search by name to restore original product list without the need to refetch
- Improve image lazy loading and add some color background if image is not available
- Add proper error boundary handling within the app
- Improve loader and give feedback when products are fetching or doing something in the background
- Improve layout for better experience

**Deploy**

https://sample-4o8vy8n3n-abfx.vercel.app/

